#!/usr/bin/env python3
import json
import os
import sys
import time
import tkinter as tk
import tkinter.messagebox
from pathlib import Path

import HardwareIface
import P8
import ScoreboardInterlink
from InternetIface import InternetIface


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        row = 0
        self.main_label = tk.Label(self, text='JETT Control')
        self.main_label.grid(column=0, columnspan=4, row=row, sticky='wens')
        row += 1

        self.scoreboard_address_label = tk.Label(self, text='Scoreboard Address:')
        self.scoreboard_address_label.grid(column=0, row=row, sticky='w')
        self.scoreboard_address_text = tk.Text(self, height=1, width=20)
        self.scoreboard_address_text.grid(column=1, row=row, sticky='we')
        self.scoreboard_connect_button = tk.Button(self, text="Connect To Scoreboard", command=self.scoreboard_connect)
        self.scoreboard_connect_button.grid(column=2, row=row, sticky='we')
        row += 1

        self.beacon_address_label = tk.Label(self, text='Beacon Address:')
        self.beacon_address_label.grid(column=0, row=row, sticky='w')
        self.beacon_address_text = tk.Text(self, height=1, width=20)
        self.beacon_address_text.grid(column=1, row=row, sticky='we')
        self.beacon_connect_button = tk.Button(self, text="Connect To Beacon", command=self.beacon_connect)
        self.beacon_connect_button.grid(column=2, row=row, sticky='we')
        row += 1

        self.times_frame = tk.Frame(self)
        self.times_frame.grid(column=0, row=row, columnspan=7, sticky='nswe')
        self.times_frame.columnconfigure(0, weight=1)
        self.times_frame.columnconfigure(1, weight=1)
        self.times_frame.columnconfigure(2, weight=1)
        self.times_frame.columnconfigure(3, weight=1)
        self.lineup_time = tk.StringVar()
        self.lineup_time.set(P8.jam_start_time / 1000)
        self.lineup_time_label = tk.Label(self.times_frame, text='Lineup\nTime:')
        self.lineup_time_label.grid(column=0, row=0, sticky='e')
        self.lineup_time_spinner = tk.Spinbox(self.times_frame, from_=20, to=120, width=4,
                                              textvariable=self.lineup_time, command=self.set_time_lineup)
        self.lineup_time_spinner.grid(column=1, row=0, sticky='nswe')
        self.timeout_time_label = tk.Label(self.times_frame, text='Timeout\nTime:')
        self.timeout_time_label.grid(column=2, row=0, sticky='e')
        self.timeout_time = tk.StringVar()
        self.timeout_time.set(P8.the_tto_time/1000)
        self.timeout_time_spinner = tk.Spinbox(self.times_frame, from_=60, to=300, width=4,
                                               textvariable=self.timeout_time, command=self.set_time_timeout)
        self.timeout_time_spinner.grid(column=3, row=0, sticky='nswe')
        row += 1

        self.volume_whistle_value = tk.StringVar()
        self.volume_whistle_value.set("30")
        self.volume_voice_value = tk.StringVar()
        self.volume_voice_value.set("30")
        self.volume_frame = tk.Frame(self)
        self.volume_frame.grid(column=0, row=row, columnspan=4, sticky='we')
        self.volume_frame.grid_rowconfigure(0, weight=1)
        self.volume_frame.grid_columnconfigure(0, weight=1)
        self.volume_frame.grid_columnconfigure(1, weight=1)
        self.volume_frame.grid_columnconfigure(2, weight=1)
        self.volume_frame.grid_columnconfigure(3, weight=1)
        self.volume_question = tk.Label(self.volume_frame, text='Whistle\nVolume')
        self.volume_question.grid(column=0, row=0, sticky='e')
        self.volume_whistle = tk.Spinbox(self.volume_frame, from_=0, to=30, width=3,
                                         textvariable=self.volume_whistle_value, command=self.set_volume_whistle)
        self.volume_whistle.grid(column=1, row=0, sticky='nswe')
        self.volume_question = tk.Label(self.volume_frame, text='Voice\nVolume')
        self.volume_question.grid(column=3, row=0, sticky='e')
        self.volume_voice = tk.Spinbox(self.volume_frame, from_=0, to=30, width=3, textvariable=self.volume_voice_value,
                                       command=self.set_volume_speech)
        self.volume_voice.grid(column=4, row=0, sticky='nswe')
        row += 1

        self.led_frame = tk.Frame(self)
        self.led_frame.grid(row=row, column=0, columnspan=7,sticky='nsew')
        self.led_frame.grid_rowconfigure(0, weight=1)
        self.led_frame.grid_columnconfigure(0, weight=0)
        self.led_frame.grid_columnconfigure(1, weight=1)
        self.led_frame.grid_columnconfigure(2, weight=2)
        self.countmode = tk.StringVar()
        self.countmode.set("Down")
        self.count_mode_label = tk.Label(self.led_frame, text='Count Mode:')
        self.count_mode_label.grid(column=0, row=0, sticky='w')
        self.count_mode_dropdown = tk.OptionMenu(self.led_frame, self.countmode,
                                                 *["Down", "Up", "Off", "OneDown", "OneOff"],
                                                 command=self.change_countmode)
        self.count_mode_dropdown.grid(column=1, row=0, sticky='nswe')
        self.leds_when_calling = tk.IntVar()
        self.leds_when_calling.set(0)
        self.leds_when_calling_checkbox = tk.Checkbutton(self.led_frame, variable=self.leds_when_calling,
                                                         text='LEDs When Calling Jam?', command=self.change_call)
        self.leds_when_calling_checkbox.grid(column=2, row=0, sticky='w')
        row += 1

        self.empty_frame_a = tk.Frame(self)
        self.empty_frame_a.grid(column=0, row=row, columnspan=4, sticky='wens')
        self.override_label = tk.Label(self.empty_frame_a, text='')
        self.override_label.grid(column=0, row=0, sticky='wens')
        row += 1

        # contains the goto 5 button for now:
        self.override_frame = tk.Frame(self)
        self.override_frame.grid(column=0, row=row, columnspan=4, sticky='wens')
        self.override_frame.grid_rowconfigure(0, weight=1)
        self.override_frame.grid_columnconfigure(0, weight=1)
        self.goto5_button = tk.Button(self.override_frame, text='Goto 5', command=self.goto5)
        self.goto5_button.grid(column=0, row=0, sticky='ewns')
        row += 1

        self.empty_frame_1 = tk.Frame(self)
        self.empty_frame_1.grid(column=0, row=row, columnspan=4, sticky='wens')
        self.override_label_1 = tk.Label(self.empty_frame_1, text='')
        self.override_label_1.grid(column=0, row=0, sticky='wens')
        row += 1

        self.ops_frame = tk.Frame(self)
        self.ops_frame.grid(column=0, row=row, columnspan=4, sticky='wens')
        self.ops_frame.grid_rowconfigure(0, weight=1)
        self.ops_frame.grid_columnconfigure(0, weight=1)
        self.ops_frame.grid_columnconfigure(1, weight=1)
        self.ops_frame.grid_columnconfigure(2, weight=1)
        self.quitButton = tk.Button(self.ops_frame, text='Quit', command=self.end)
        self.quitButton.grid(column=0, row=0, sticky='ewns')
        self.saveButton = tk.Button(self.ops_frame, text='Save', command=self.save)
        self.saveButton.grid(column=1, row=0, sticky='ewns')
        self.aboutButton = tk.Button(self.ops_frame, text='About', command=self.about)
        self.aboutButton.grid(column=2, row=0, sticky='ewns')
        row += 1

        self.grid()
        self.sb_connection = None
        self.scoreboard_connected = False
        self.beacon_connection = None
        self.beacon_connected = False

        self.load()
        return

    def goto5(self):
        ScoreboardInterlink.go_to_5 = True
        ScoreboardInterlink.go_to_5_whenrequest = time.time()
        ScoreboardInterlink.go_to_5_processed = False
        return

    def set_volume_speech(self):
        HardwareIface.hardware_message_queue[0].push(f'vols {self.volume_voice_value.get()}')
        return

    def set_volume_whistle(self):
        HardwareIface.hardware_message_queue[0].push(f'volw {self.volume_whistle_value.get()}')
        return

    def set_time_lineup(self):
        P8.jam_start_time = 1000*int(self.lineup_time.get())
        P8.lineup_warning_time = P8.jam_start_time - 5000
        return

    def set_time_timeout(self):
        P8.the_tto_time = 1000*int(self.timeout_time.get())
        return

    def change_countmode(self, newtype):
        P8.count_command = f'count{newtype.lower()}'
        return

    def scoreboard_connect(self):
        if not self.scoreboard_connected:
            # TODO: work out how to close scoreboard
            self.sb_connection = ScoreboardInterlink.SBConnectThread({
                'url': self.scoreboard_address_text.get("1.0", "end-1c"),
                'on_message': P8.on_message,
                'on_error': P8.on_error,
                'on_close': P8.on_close,
                'on_open': P8.on_open,
            })
            self.sb_connection.start()
            self.scoreboard_connected = True
        else:
            self.sb_connection.close()
            self.scoreboard_connected = False
        self.scoreboard_connect_button['text'] = \
            'Disconnect From Scoreboard' if self.scoreboard_connected else 'Connect To Scoreboard'
        return

    def beacon_connect(self):
        if not self.beacon_connected:
            self.beacon_connection = InternetIface({
                'host': f'{self.beacon_address_text.get("1.0", "end-1c")}',
                'port': 23,
                'volw': int(self.volume_whistle_value.get()),
                'vols': int(self.volume_voice_value.get())
            })
            self.beacon_connection.start()
            self.beacon_connected = True
        else:
            self.beacon_connection.stop()
            self.beacon_connected = False
        self.beacon_connect_button['text'] = \
            'Disconnect From Beacon' if self.scoreboard_connected else 'Connect To Beacon'
        return

    def end(self):
        if tkinter.messagebox.askokcancel("Are you sure?", "Are you sure you wish to quit?"):
            P8.keep_running = False
            if self.scoreboard_connected:
                self.sb_connection.close()
            if self.beacon_connected:
                self.beacon_connection.stop()
            self.quit()

    def change_call(self):
        status = self.leds_when_calling.get() > 0
        P8.call_command = 'callled' if status else 'call'
        return

    def save(self):
        save_object = {
            'beacon': self.beacon_address_text.get("1.0", "end-1c"),
            'scoreboard': self.scoreboard_address_text.get("1.0", "end-1c"),
            'lineup': int(P8.jam_start_time / 1000),
            'timeout': int(P8.the_tto_time/1000),
            'volume_speaking': self.volume_voice_value.get(),
            'volume_whistling': self.volume_whistle_value.get(),
            'count': self.countmode.get(),
            'call_with_leds': self.leds_when_calling.get() > 0
        }
        file = Path.home().joinpath(".p8cfg").open("w+")
        json.dump(save_object, file)
        file.close()

    @staticmethod
    def set_text(obj, value):
        obj.delete("1.0", "end")
        obj.insert("1.0", value)

    def load(self):
        if not Path.home().joinpath(".p8cfg").exists():
            return
        file = Path.home().joinpath(".p8cfg").open("r")
        saved_object = json.load(file)
        file.close()
        if 'beacon' in saved_object:
            self.set_text(self.beacon_address_text, saved_object['beacon'])
        if 'scoreboard' in saved_object:
            self.set_text(self.scoreboard_address_text, saved_object['scoreboard'])
        if 'lineup' in saved_object:
            self.lineup_time.set(saved_object['lineup'])
            self.set_time_lineup()
        if 'timeout' in saved_object:
            self.timeout_time.set(saved_object['timeout'])
            self.set_time_timeout()
        if 'volume_whistling' in saved_object:
            self.volume_whistle_value.set(saved_object['volume_whistling'])
            self.set_volume_whistle()
        if 'volume_speaking' in saved_object:
            self.volume_voice_value.set(saved_object['volume_speaking'])
            self.set_volume_speech()
        if 'count' in saved_object:
            self.countmode.set(saved_object['count'])
        if 'call_with_leds' in saved_object:
            self.leds_when_calling.set( saved_object['call_with_leds'] > 0)
        return

    def about(self):
        tkinter.messagebox.showinfo("About JETT", "JETT by Vector (Western Australia Roller Derby)")


if __name__ == "__main__":
    try:
        # if we are running inside a pyinstaller package, we display a splashscreen while we are
        # unzipping - and thus, we should close it once we get started
        import pyi_splash
        pyi_splash.close()
    except:
        pass

    app = Application()
    app.master.title('JETT')
    # join with sys._MEIPASS is needed to access a file path inside a pyinstaller bundle:
    if hasattr(sys, '_MEIPASS'):
        app.master.iconphoto(True, tk.PhotoImage(file=os.path.join(sys._MEIPASS,'icon.png')))
    else:
        app.master.iconphoto(True, tk.PhotoImage(file='icon.png'))
    app.mainloop()
    exit(0)
    # call_command = 'call'
