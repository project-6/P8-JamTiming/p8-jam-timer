# It is currently intended that each of the serial and internet based devices will talk in a similar manner, so try and
# write a super class containing stuff each descendent will need to deal with...
import selectors
import threading
import time

import ScoreboardInterlink

class HardwareOutQ():
    def __init__(self):
        self.lock = threading.Lock()
        self.q = []
        self.last_sent = 0

    def push(self, message):
        self.lock.acquire(True)
        self.q.append(message)
        self.lock.release()
        return

    def pop(self):
        the_time = time.monotonic()
        # if (the_time - self.last_sent) < 0.15:
        #     # Throttle to 1 message every 200mS MAX or put another way:
        #     # Memo to hardwareIface - you have 150mS to send that message or else
        #     return None
        self.lock.acquire(True)
        if len(self.q) > 0:
            return_value = self.q.pop()
            self.last_sent = the_time
        else:
            return_value = None
        self.lock.release()
        return return_value


hardware_message_queue = [
    HardwareOutQ()
]

class HardwareIface(threading.Thread):
    def __init__(self, connection_info):
        threading.Thread.__init__(self)
        # Whatever the other implementations do, they need to set self.keepRunning = true
        # and set box_list to an array of ChairGUIs
        self.connection_info = connection_info
        self.stream = None
        self.keepRunning = None
        self.setDaemon(True)
        self.whoami = 0
        self.selector = selectors.DefaultSelector()
        self.tag_enroller = None
        # the next 2 are to facilitate other threads blocking on receiving a reply
        self.condition_done = threading.Condition()
        if connection_info['volw']:
            self.volw = connection_info['volw']
            hardware_message_queue[self.whoami].push(f'volw {self.volw}')
        if connection_info['vols']:
            self.vols = connection_info['vols']
            hardware_message_queue[self.whoami].push(f'vols {self.vols}')
        return

    def connect(self):
        # this is actually a "virtual" method if we had C/C++/Java at our disposal
        # we expect that self.stream is something that read / write / close can use
        raise NotImplementedError()

    def disconnect(self):
        raise NotImplementedError()

    def signal_stop(self):
        self.keepRunning = False
        return

    # returns a string that we can blindly insert into logs and know we have provided enough info
    def connection_description(self):
        raise NotImplementedError()

    def readln(self):
        raise NotImplementedError()

    def println(self, thing):
        raise NotImplementedError()

    def log(self, comment):
        print(self.connection_description(), comment)
        return

    def read(self):
        try:
            import P8
            line = self.readln().lower()
            self.log(line)
            # TODO: In phase 3, we disconnect the buttons from manual initiation and use them
            # to tell us to tell the scoreboard to do the things. The whistling will happen
            # once the scoreboard tells us that the thing has actually been actioned.
            if '! call' in line:
                P8.send(ScoreboardInterlink.call_off)
            elif '! ends' in line:
                pass
            elif '! start' in line:
                ScoreboardInterlink.go_to_5 = True
                ScoreboardInterlink.go_to_5_whenrequest = time.time()
                ScoreboardInterlink.go_to_5_processed = False
            elif '! timeout' in line:
                P8.send(ScoreboardInterlink.official_timeout)
            elif '! ping' in line:
                # this is a keep alive message - ignore
                pass
            elif '! tto1' in line:
                P8.send(ScoreboardInterlink.tto_1)
            elif '! tto2' in line:
                P8.send(ScoreboardInterlink.tto_2)
            elif '! oto' in line:
                P8.send(ScoreboardInterlink.official_timeout)
            elif '! or1' in line:
                P8.send(ScoreboardInterlink.or_1)
            elif '! or2' in line:
                P8.send(ScoreboardInterlink.or_2)
            elif '! done' in line:
                # unblock the main thread if it's blocked on the whistle.
                with self.condition_done:
                    self.condition_done.notifyAll()
        except IOError as blerg_io:
            self.disconnect()
            error_string = 'Error: ' + str(blerg_io.args)
            self.log(error_string)
            time.sleep(1)
            self.connect()
        except ValueError as blerg_v:
            self.log('Bad data ' + str(blerg_v.args))
            self.disconnect()
            time.sleep(1)
            self.connect()

    def wait_done(self):
        with self.condition_done:
            self.condition_done.wait()
        return

    def write(self):
        message = hardware_message_queue[self.whoami].pop()
        while message:
            self.log(message)
            self.println(message)
            self.log('Sending:' + message)
            message = hardware_message_queue[self.whoami].pop()
        return

    def selector_callback(self, sock, mask):
        if mask & selectors.EVENT_READ:
            self.read()
        if mask & selectors.EVENT_WRITE:
            self.write()
        return

    def stop(self):
        self.keepRunning = False

    def run(self):
        self.connect()
        if not self.keepRunning:
            self.log('Thread closing')
            self.disconnect()
            return
        time.sleep(1)
        # the use of a selector is a bit of a kludge, but designed to "break" the blocking often enough that sending
        # commands back to the device is timely. That and reduce the debug distance reading frequency RIGHT DOWN
        self.selector.register(self.descriptor, selectors.EVENT_READ | selectors.EVENT_WRITE, self.selector_callback)
        while self.keepRunning:
            self.selector.select(timeout=0.1)
            events = self.selector.select()
            for key, mask in events:
                key.data(key.fileobj, mask)
