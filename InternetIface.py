import socket

from HardwareIface import HardwareIface

class InternetIface(HardwareIface):
    def __init__(self, connection_info):
        self.descriptor = None
        HardwareIface.__init__(self, connection_info)
        self.host = connection_info['host']
        if 'port' in connection_info:
            self.port = int(connection_info['port'])
        else:
            self.port = 23

    def connect(self):
        # this is actually a "virtual" method if we had C/C++/Java at our disposal
        # we expect that self.stream is something that read / write / close can use
        self.log('Attempting to connect')
        try:
            self.descriptor = socket.create_connection((self.host, self.port))
            # self.stream = self.descriptor.makefile("wr")
            self.keepRunning = True
        except IOError as blerg_io:
            self.log('Blerg (closing): ' + str(blerg_io.args))
            self.keepRunning = False

    def disconnect(self):
        if self.descriptor:
            self.descriptor.close()

    def readln(self):
        c = None
        rv = ''
        c = self.descriptor.recv(1)
        while c and c.decode(encoding='ascii', errors='ignore') != '\n':
            the_char = c.decode(encoding='ascii', errors='ignore')
            rv += the_char
            c = self.descriptor.recv(1)
        return rv.strip()

    def println(self, thing):
        return self.descriptor.send((thing + '\n').encode(encoding='ascii', errors='ignore'))

    # returns a string that we can blindly insert into logs and know we have provided enough info
    def connection_description(self):
        if 'port' in self.connection_info:
            return self.connection_info['host'] + ':' + str(self.connection_info['port'])
        return self.connection_info['host']
