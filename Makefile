MKINSTALL=pyinstaller
all:  stuff

stuff: GUI.py HardwareIface.py InternetIface.py P8.py ScoreboardInterlink.py SerialIface.py icon.png
	$(MKINSTALL) GUI.py -d all --add-data="icon.png:." --splash icon.png --onefile -n P8-jett 

clean:
	-rm -r dist/
	-rm *.pyo
