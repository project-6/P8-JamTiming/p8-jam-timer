import getopt
import json
import sys
import threading
from time import sleep
import websocket
import ScoreboardInterlink
from HardwareIface import hardware_message_queue
from InternetIface import InternetIface
from ScoreboardInterlink import registration, ping, start_jam

keep_running = True
booting = True
args = {}

team_colours = ['Black', 'White']

lineup_warning_time = 25000
# the lineup time at which we say "5 SECONDS"
jam_start_time = 30000
# the lineup time at which we start the jam

timeout_owner = None

the_tto_time = 60000

call_command = 'call'
count_command = 'countdown'

period_number = 0


def on_error(ws, error):
    print('!!! ERROR:', error)
    return


def on_close(ws):
    print("### closed ###")
    return


def on_open(ws):
    class KeepAlive(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)

        def run(self):
            count = 0
            while keep_running:
                sleep(1)
                if count % 25 == 0 and count:
                    ws.send(ping)
                    count = 0
                else:
                    count += 1

    ws.send(json.dumps(registration))
    KeepAlive().start()


def lineup_warning():
    # the visual equivalent of "5 seconds"
    # FIXME: command line argument for up, down, oneup, onedown, off
    hardware_message_queue[0].push(count_command)
    return


def whistle_on():
    hardware_message_queue[0].push('start')
    return


def whistle_off(two_minutes=False):
    hardware_message_queue[0].push(call_command if two_minutes else call_command + 'short')
    return


def whistle_timeout(colour):
    command = ''
    if colour == 'officials':
        command = 'oto'
    else:
        command = 'tto ' + colour
    hardware_message_queue[0].push(command)
    e_whistle_connection.wait_done()
    return


def whistle_rolling():
    hardware_message_queue[0].push('ends')
    e_whistle_connection.wait_done()
    return


def official_review_announce(colour):
    command = 'or ' + colour
    hardware_message_queue[0].push(command)
    e_whistle_connection.wait_done()
    return


def official_review_outcome(successful):
    if successful:
        hardware_message_queue[0].push('oryes')
    else:
        hardware_message_queue[0].push('orno')
    return


def period_status(type):
    if type == 'half':
        hardware_message_queue[0].push("halftime")
    elif type == 'full':
        hardware_message_queue[0].push("fulltime")
    elif type == 'confirmed':
        hardware_message_queue[0].push("gameover")
    return


def on_message(ws, message):
    global booting
    global timeout_owner
    global period_number
    ScoreboardInterlink.sb_connection = ws
    print(message)
    response = json.loads(message)
    if 'state' not in response:
        return
    state = response['state']
    if 'WS.Client.Id' in state:
        return
    if booting:
        booting = False
        if "ScoreBoard.CurrentGame.Clock(Period).Number" in state:
            period_number = state["ScoreBoard.CurrentGame.Clock(Period).Number"]
        return
    keys = []
    for key in state:
        print(key)
        keys.append(key)

    # Some subtles changes have creeped in, we need to start sorting the keys
    for key in sorted(keys, key=lambda e: (e != "ScoreBoard.CurrentGame.InJam", e)):
        print(key)
        if key == 'ScoreBoard.CurrentGame.InJam':
            if state[key]:
                print('Whistle on')
                whistle_on()
                break
            else:
                print('Whistle off')
                #
                # the second clause is superfluous - the key ScoreBoard.CurrentGame.Clock(Jam).Time
                # is only included in the state information if the jam ends after 2 minutes
                whistle_off("ScoreBoard.CurrentGame.Clock(Jam).Time" in state and state[
                    "ScoreBoard.CurrentGame.Clock(Jam).Time"] == 0)
        elif key == 'ScoreBoard.CurrentGame.Clock(Timeout).Running':
            owner = state.get('ScoreBoard.CurrentGame.TimeoutOwner')
            if len(owner) and owner != 'O':
                owner = owner.split('_')[1]
            is_or = 'ScoreBoard.CurrentGame.OfficialReview' in state
            if state['ScoreBoard.CurrentGame.Clock(Timeout).Running']:
                if is_or:
                    if owner == '1':
                        print('OR1 - sending')
                        official_review_announce(team_colours[0])
                    elif owner == '2':
                        print('OR2 - sending')
                        official_review_announce(team_colours[1])
                else:
                    if owner == 'O':
                        timeout_owner = 'O'
                        whistle_timeout('officials')
                    elif owner == '1':
                        print('TTO1 - sending')
                        timeout_owner = '1' if not is_or else 'O'
                        whistle_timeout(team_colours[0])
                    elif owner == '2':
                        print('TTO2 - sending')
                        timeout_owner = '2' if not is_or else 'O'
                        whistle_timeout(team_colours[1])
            else:
                print('Rolling')
                whistle_rolling()
            break
        elif key == 'ScoreBoard.CurrentGame.Clock(Lineup).Time':
            lineup_current_time = state['ScoreBoard.CurrentGame.Clock(Lineup).Time']
            if (not ScoreboardInterlink.go_to_5 and lineup_current_time == lineup_warning_time) \
                    or (ScoreboardInterlink.go_to_5 and not ScoreboardInterlink.go_to_5_processed):
                # ScoreboardInterlink.go_to_5 is just a flag to signal to the websocket listener that
                # we need to tell the JETT to go to 5
                if ScoreboardInterlink.go_to_5:
                    ScoreboardInterlink.go_to_5_processed = True
                    ScoreboardInterlink.go_to_5_start_time = lineup_current_time + 5000
                lineup_warning()
            elif lineup_current_time == jam_start_time \
                    or (ScoreboardInterlink.go_to_5
                        and lineup_current_time == ScoreboardInterlink.go_to_5_start_time
            ):
                ws.send(start_jam)
                ScoreboardInterlink.go_to_5 = False
                ScoreboardInterlink.go_to_5_start_time = 0
            else:
                continue
        elif key == 'ScoreBoard.CurrentGame.Team(2).RetainedOfficialReview':
            if state['ScoreBoard.CurrentGame.Team(2).RetainedOfficialReview']:
                official_review_outcome(True)
            else:
                official_review_outcome(False)
        elif key == 'ScoreBoard.CurrentGame.Team(1).RetainedOfficialReview':
            if state['ScoreBoard.CurrentGame.Team(1).RetainedOfficialReview']:
                official_review_outcome(True)
            else:
                official_review_outcome(False)
        elif key == 'ScoreBoard.CurrentGame.Clock(Intermission).Running':
            print('Intermission edge')
            if not state['ScoreBoard.CurrentGame.Clock(Intermission).Running']:
                whistle_rolling()
                # end of intermission
            else:
                print('Period number is ', period_number)
                sleep(3)
                period_status('half' if period_number < 1 else 'full')
                period_number += 1
        elif key == 'ScoreBoard.CurrentGame.Clock(Period).Number':
            # the period has started
            period_number = int(state[key])
            print('Period set to ', period_number)
            if state.get('ScoreBoard.CurrentGame.Clock(Lineup).Running', False):
                whistle_rolling()
        elif key == 'ScoreBoard.CurrentGame.OfficialScore':
            if state[key]:
                period_status('confirmed')
            else:
                period_status('full')
        elif key == 'ScoreBoard.CurrentGame.Team(1).Name':
            team_colours[0] = state[key]
        elif key == 'ScoreBoard.CurrentGame.Team(2).Name':
            team_colours[1] = state[key]
        elif key == 'ScoreBoard.CurrentGame.Clock(Timeout).Time':
            if timeout_owner in ['1', '2']:
                if state[key] == the_tto_time:
                    send(ScoreboardInterlink.call_off)
        elif key == "ScoreBoard.CurrentGame.Game":
            print(key, state[key])
    return


def start_sb():
    ScoreboardInterlink.sb_connection = websocket.WebSocketApp(args['b'][0],
                                                               on_message=on_message,
                                                               on_error=on_error,
                                                               on_close=on_close)
    ScoreboardInterlink.sb_connection.on_open = on_open
    ScoreboardInterlink.sb_connection.run_forever()


def send(message) -> None:
    class Sender(threading.Thread):
        def __init__(self, ws, message_text):
            threading.Thread.__init__(self)
            self.ws = ws
            self.message_text = message_text

        def run(self) -> None:
            self.ws.send(self.message_text)

    Sender(ScoreboardInterlink.sb_connection, message).start()
    return


def usage():
    print('Usage is ' + sys.argv[0])
    print('\t-b scoreboard websockets URL')
    print('\t-d soundfile directory')
    print('\t-l line up time (in seconds)')
    print('\t-T team timeout up time (in seconds)')
    print('\t-e path to USB/Serial based Timing Beacon')
    print('\t-E IP address of WIFI-based Timing Beacon')
    print('\t-C activate all LEDs during call off')
    print('\t-5 count mode - one of up,down,off,oneup,onedown')
    print('\t-v Speak volume')
    print('\t-V Whistle volume')
    return


if __name__ == "__main__":
    opt_list, arg_list = getopt.getopt(sys.argv[1:], "b:d:l:e:E:T:C5:v:V:")
    # print(opt_list)
    for item in opt_list:
        k = item[0].replace("-", "")
        for the_arg in item[1:]:
            if k in args:
                args[k].append(the_arg)
            else:
                args[k] = [the_arg]

    if 'C' in args:
        call_command = 'callled'

    if '5' in args:
        if 'up' in args['5']:
            count_command = 'countup'
        elif 'off' in args['5']:
            count_command = 'countoff'
        elif 'oneup' in args['5']:
            count_command = 'countoneup'
        elif 'onedown' in args['5']:
            count_command = 'countonedown'

    if 'b' not in args:
        # scoreboard URL
        args['b'] = ['ws://localhost:8000/WS']

    if 'l' in args:
        # lineup time
        the_lineup_time = int(args['l'][0])
        if the_lineup_time < 5:
            print('Lineup time must be greater than 5 seconds')
            exit(0)
        jam_start_time = int(args['l'][0]) * 1000
        lineup_warning_time = jam_start_time - 5000

    if 'T' in args:
        # team timeout time
        the_tto_time = int(args['T'][0]) * 1000

    vols = None
    volw = None
    if 'V' in args:
        volw = int(args['V'][0])
    if 'v' in args:
        vols = int(args['v'][0])

    e_whistle_connection = None
    if 'E' in args:
        # use e-whistle (internet based)
        e_whistle_connection = InternetIface({
            'host': args['E'][0],
            'port': 23,
            'volw': volw,
            'vols': vols
        })
        e_whistle_connection.start()

    start_sb()
