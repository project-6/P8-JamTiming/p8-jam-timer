# We use this file to contain our discoveries on how to get CRG to do various things.

# These ones are used:
import threading
import websocket

ping = '{"action": "Ping"}'  # not sure if still needed, but the web browsers are all doing it
start_jam = '{"action":"Set","key":"ScoreBoard.CurrentGame.StartJam","value":true,"flag":""}'

# These ones are for P5 (scoring gauntlet)
trip_points = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).TripScore","value":4,"flag":""}'
trip_add = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).AddTrip","value":true,"flag":""}'
lead_awarded = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).Lead","value":true,"flag":""}'
lead_lost = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).Lost","value":true,"flag":""}'
star_pass = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(2).StarPass","value":true,"flag":""}'

call_off = '{"action":"Set","key":"ScoreBoard.CurrentGame.StopJam","value":true,"flag":""}'
official_timeout = '{"action":"Set","key":"ScoreBoard.CurrentGame.OfficialTimeout","value":true,"flag":""}'

tto_1 = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).Timeout","value":true,"flag":""}'
tto_2 = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(2).Timeout","value":true,"flag":""}'

or_1 = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(1).OfficialReview","value":true,"flag":""}'
or_2 = '{"action":"Set","key":"ScoreBoard.CurrentGame.Team(2).OfficialReview","value":true,"flag":""}'

sb_connection = None

# Used to convert the "green" button into a "go to 5" button
go_to_5 = False
go_to_5_start_time = 0
go_to_5_whenrequest = 0
go_to_5_processed = False

registration = {
    "action": "Register",
    "paths": [
        "ScoreBoard.CurrentGame.Clock(*).Running",
        "ScoreBoard.CurrentGame.Clock(*).Direction",
        "ScoreBoard.CurrentGame.InJam",
        "ScoreBoard.CurrentGame.Team(1).Score",
        "ScoreBoard.CurrentGame.Team(1).JamScore",
        "ScoreBoard.CurrentGame.InJam",
        "ScoreBoard.CurrentGame.Team(2).Score",
        "ScoreBoard.CurrentGame.Team(2).JamScore",
        "ScoreBoard.CurrentGame.Clock(Period).Name",
        "ScoreBoard.Game(*).Clock(Period).Number",
        "ScoreBoard.CurrentGame.Clock(Period).Number",
        "ScoreBoard.CurrentGame.Clock(Period).Time",
        "ScoreBoard.CurrentGame.Clock(Jam).Name",
        "ScoreBoard.CurrentGame.Clock(Jam).Number",
        "ScoreBoard.CurrentGame.Clock(Jam).Time",
        "ScoreBoard.CurrentGame.Clock(Lineup).Name",
        "ScoreBoard.CurrentGame.Clock(Lineup).Running",
        "ScoreBoard.CurrentGame.Clock(Timeout).Name",
        "ScoreBoard.CurrentGame.Clock(Timeout).Running",
        "ScoreBoard.CurrentGame.TimeoutOwner",
        "ScoreBoard.CurrentGame.OfficialReview",
        "ScoreBoard.CurrentGame.Team(1).Timeouts",
        "ScoreBoard.CurrentGame.Team(1).RetainedOfficialReview",
        "ScoreBoard.CurrentGame.Team(2).Timeouts",
        "ScoreBoard.CurrentGame.Team(2).OfficialReviews",
        "ScoreBoard.CurrentGame.Team(2).RetainedOfficialReview",
        "ScoreBoard.CurrentGame.Clock(Lineup).Time",
        "ScoreBoard.CurrentGame.Clock(Timeout).Time",
        "ScoreBoard.CurrentGame.OfficialScore",
        "ScoreBoard.CurrentGame.Clock(Intermission).Time",
        "ScoreBoard.CurrentGame.Team(1).Color",
        "ScoreBoard.CurrentGame.Team(2).Color",
        "ScoreBoard.CurrentGame.NoMoreJam",
        "ScoreBoard.CurrentGame.Clock(*).Running",
        "ScoreBoard.CurrentGame.Clock(*).Direction",
        "ScoreBoard.CurrentGame.InJam",
        "ScoreBoard.CurrentGame.TimeoutOwner",
        "ScoreBoard.CurrentGame.OfficialReview",
        "ScoreBoard.CurrentGame.Clock(Lineup).Time",
        "ScoreBoard.CurrentGame.Clock(Timeout).Time",
        "ScoreBoard.CurrentGame.Clock(Intermission).Number",
        "ScoreBoard.CurrentGame.Clock(Intermission).Time",
        "ScoreBoard.CurrentGame.Team(1).Name",
        "ScoreBoard.CurrentGame.Team(2).Name",
        "ScoreBoard.CurrentGame.Team(1).DisplayLead",
        "ScoreBoard.CurrentGame.Team(2).DisplayLead",
    ]
}


class SBConnectThread(threading.Thread):
    def __init__(self, connection_info):
        threading.Thread.__init__(self)
        self.sb_connection = websocket.WebSocketApp(f'ws://{connection_info["url"]}/WS',
                                                    on_message=connection_info['on_message'],
                                                    on_error=connection_info['on_error'],
                                                    on_close=connection_info['on_close'])
        self.sb_connection.on_open = connection_info['on_open']

    def run(self):
        self.sb_connection.run_forever()

    def close(self):
        self.sb_connection.close()
